## secure

* **CTF:** Redpwn CTF 2021 
* **Category:** web
* **Points:** 105
* **Author(s):** b0uldrr
* **Date:** 11/07/21

---

### Challenge
```
Just learned about encryption-now, my website is unhackable!

secure.mc.ax
```

### Downloads
* [index.js](index.js) 

---

### Solution

Visiting the above url, we are presented with a login page:
![login](images/login.png)

After reading the source code of `index.js`, we can see it does the following: 

1. Upon loading, the app creates a new database table `users`
```javascript
db.exec(`DROP TABLE IF EXISTS users;`);
db.exec(`CREATE TABLE users(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT,
    password TEXT
);`);
```
2. Adds a new user `admin` with a randomly generated password. Both are base64 encoded before being stored in the database.
```javascript
db.exec(`INSERT INTO users (username, password) VALUES (
    '${btoa('admin')}',
    '${btoa(crypto.randomUUID)}'
)`);
```
3. Upon a user submitting a username and password, the app base64 encodes the username and password.
```javascript
const username = document.createElement('input');
        username.setAttribute('name', 'username');
        username.setAttribute('value',
          btoa(document.querySelector('#username').value)
        );

        const password = document.createElement('input');
        password.setAttribute('name', 'password');
        password.setAttribute('value',
          btoa(document.querySelector('#password').value)
        );
```
4. The app checks the base64 encoded user submitted username and password against the base64 encoded username and password in the database.
```js
const query = `SELECT id FROM users WHERE
          username = '${req.body.username}' AND
          password = '${req.body.password}';`;
```

We can see in step 5 above that the user input is used directly in the SQL query without sanitation, making it vulnerable to traditional SQL Injection attacks. However, because the app encodes our input as base64 before posting, our attack payload is rendered inert. If we bypass the app with a proxy (like Burp Suite, or with a direct Curl request) then we can submit our payload in plain text and this will still be processed by the app.

My attack plan:
1. Start Burp Suite
2. Try to login using the app front end, with username `admin` and password `test`
3. Capture that POST request in Burp and send it to the Repeater
4. Change the password value in the POST request to a standard SQL injection attack `1' OR '1'='1`
5. We successfully authenticate and the flag is returned:

![flag](images/flag.png)

---

### Flag 
```
flag{50m37h1n6_50m37h1n6_cl13n7_n07_600d}
```
