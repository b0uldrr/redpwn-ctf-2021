## Redpwn CTF 2021

* **CTF Time page:** https://ctftime.org/event/1327 
* **Category:** jeopardy
* **Date:**  Fri, 09 July 2021, 19:00 UTC — Mon, 12 July 2021, 19:00 UTC

---

### Solved Challenges
| Name | Category | Points | Key Concepts |
|------|----------|--------|--------------|
|beginner-generic-pwn-number-0|pwn|105|bof, 64-bit|
|printf-please|pwn|108|printf, 64-bit|
|ret2generic-flag-reader|pwn|106|bof, 64-bit|
|ret2the-unknown|pwn|110|bof, ret2libc, ROP, 64-bit|
|secure|web|105|javascript, sql injection, burp suite|

